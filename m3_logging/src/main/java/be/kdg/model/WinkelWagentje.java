package be.kdg.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class WinkelWagentje {
    private List<Product> wagentje;
    private double saldo;
    private static final Logger logger = Logger.getLogger("be.kdg.model.WinkelWagentje");

    public WinkelWagentje() {
        wagentje = new ArrayList<>();
    }

    public void voegToe(Product product) {
        wagentje.add(product);
        saldo += product.getPrijs();
        logger.fine(String.format("Product %s toegevoegd", product.getNaam()));
    }

    public void verwijder(Product product) throws IllegalArgumentException {
        if (!wagentje.remove(product)) {
            logger.warning(String.format("Product %s niet gevonden", product.getNaam()));
        }
        saldo -= product.getPrijs();
        logger.fine(String.format("Product %s verwijderd", product.getNaam()));
    }

    public int getAantal() {
        return wagentje.size();
    }

    public double getSaldo() {
        return saldo;
    }

    public void maakWagentjeLeeg() {
        wagentje.clear();
        saldo = 0.0;
        logger.fine(String.format("Wagentje leeg gemaakt"));
    }

    public List<Product> getProductenList() {
        return Collections.unmodifiableList(wagentje);
    }
}

